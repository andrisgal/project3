﻿using System;
using System.Collections.Generic;
using static Shared.Entities;
using static Shared.Enums;

namespace BL
{
    public class Logger
    {
        private readonly List<Log> Entries;
        private readonly Interfaces.ILogRepository logRepository;

        public Logger(Interfaces.ILogRepository logRepository)
        {
            Entries = new List<Log>();
            this.logRepository = logRepository;
        }

        public void Add(Log log)
        {
            if (log.TimeStamp < DateTime.Now)
                Entries.Add(log);
        }

        public void Add(string text, Loglevels level,
            DateTime timeStamp, string userName)
        {
            Log logToAdd = new Log
            {
                Text = text,
                Level = level,
                TimeStamp = timeStamp,
                UserName = userName
            };

            Entries.Add(logToAdd);
        }

        private string CheckEmptyAndReturnUserName(string userName)
        {
            if (String.IsNullOrEmpty(userName))
                return "Anonymous";
            return userName;
        }

        public void AddDebug(string text, string userName = null)
        {
            Add(text, Loglevels.Debug, DateTime.Now,
                CheckEmptyAndReturnUserName(userName));
        }

        public void AddWarning(string text, string userName = null)
        {
            Add(text, Loglevels.Warning, DateTime.Now,
                CheckEmptyAndReturnUserName(userName));

        }

        public void AddError(string text, string userName = null)
        {
            Add(text, Loglevels.Error, DateTime.Now,
                CheckEmptyAndReturnUserName(userName));

        }

        public void Flush(bool withBulkInsert = true)
        {
            if ( EntryCount > 0)
            {
                if (withBulkInsert == true)
                    logRepository.BulkInsert(Entries);
                else
                    foreach(Log logToInsert in Entries)
                    {
                        logRepository.Insert(logToInsert);
                    }
                Entries.Clear();
            }
        }

        public bool HasDebug
        {
            get
            {
                if (Entries.Exists(x => x.Level == Loglevels.Debug))
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }



        public bool HasWarning
        {
            get
            {
                if (Entries.Exists(x => x.Level == Loglevels.Warning))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public bool HasInfo
        {
            get
            {
                if (Entries.Exists(x => x.Level == Loglevels.Info))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool HasError
        {
            get
            {
                if (Entries.Exists(x => x.Level == Loglevels.Error))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }


        }
        public int EntryCount
        {
            get
            {

                if (Entries != null)
                {
                    return Entries.Count;
                }
                else
                {
                    return 0;
                }

            }
        }

    }
}
