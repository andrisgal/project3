﻿using System.Collections.Generic;
using System.Linq;
using Interfaces;
using Shared;
using static Shared.Entities;

namespace Fakes
{
    public class FakeLogRepository : ILogRepository
    {
        private readonly List<Log> logs = new List<Log>();

        public void BulkInsert(List<Log> logs)
        {
            this.logs.AddRange(logs);
        }

        public IQueryable<Log> GetAll()
        {
            return this.logs.AsQueryable<Log>();
        }

        public void Insert(Log log)
        {
            logs.Add(log);
        }
    }
}
