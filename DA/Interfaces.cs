﻿using System.Collections.Generic;
using System.Linq;
using static Shared.Entities;

namespace Interfaces
{
    public interface ILogRepository 
    {
        IQueryable<Log> GetAll();
        void Insert(Log log);
        void BulkInsert(List<Log> logs);
    }
}
