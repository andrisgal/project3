﻿using System;
using NUnit.Framework;
using static Shared.Entities;
using static Shared.Enums;
using Fakes;
using Moq;
using System.Collections.Generic;
using System.Linq;
using BL;

namespace BLTests
{
    [TestFixture]
    public class BLTests
    {
        Mock<Interfaces.ILogRepository> mockRepo;
        Logger logger;
        List<Log> dataToAdd;
        readonly Log testLog = new Log();

        [SetUp]
        public void Init()
        {
            IList<Log> testDataBase = new List<Log>();

            dataToAdd = new List<Log>
            {
                new Log { UserName = "test1", TimeStamp = new DateTime(2019,12,12) },
                new Log { UserName = "test2", TimeStamp = new DateTime(2019,12,13) },
                new Log { UserName = "test3", TimeStamp = new DateTime(2019,12,14) },
            };
            mockRepo = new Mock<Interfaces.ILogRepository>();
            logger = new Logger(mockRepo.Object);

            mockRepo.Setup(x => x.BulkInsert(It.IsAny<List<Log>>())).Callback((List<Log> target) => testDataBase.ToList().AddRange(target));
            mockRepo.Setup(x => x.Insert(It.IsAny<Log>())).Callback((Log target) => testDataBase.Add(target));
        }

        [Test]
        public void FlushWithFakeBulkInsertRunsAsExpected()
        {
            FakeLogRepository fakeRepo = new FakeLogRepository();
            Logger loggerWithFake = new Logger(fakeRepo);

            foreach(Log data in dataToAdd)
            {
                loggerWithFake.Add(data);
            }

            loggerWithFake.Flush(true);
            Assert.AreEqual(dataToAdd, fakeRepo.GetAll().ToList());
        }

        [Test]
        public void FlushWithFakeRunsAsExpected()
        {
            FakeLogRepository fakeRepo = new FakeLogRepository();
            Logger loggerWithFake = new Logger(fakeRepo);

            foreach (Log data in dataToAdd)
            {
                loggerWithFake.Add(data);
            }

            loggerWithFake.Flush(false);
            Assert.AreEqual(dataToAdd, fakeRepo.GetAll().ToList());
        }

        [Test]
        public void FlushWithMockBulkInsertRunsOnce()
        {
            foreach (Log data in dataToAdd)
            {
                logger.Add(data);
            }
            logger.Flush(true);
            mockRepo.Verify(x => x.BulkInsert(It.IsAny<List<Log>>()), Times.Exactly(1));
        }

        [Test]
        public void FlushWithMockRunsAsExpected()
        {
            foreach (Log data in dataToAdd)
            {
                logger.Add(data);
            }
            logger.Flush(false);
            mockRepo.Verify(x => x.Insert(It.IsAny<Log>()), Times.Exactly(dataToAdd.Count));
        }

        [Test]
        public void AddIncrementsEntryCount()
        {
            logger.Add(testLog);
            Assert.AreEqual(logger.EntryCount, 1);
        }

        [Test]
        public void AddWithParamsIncrementsEntriCount()
        {
            logger.Add("Need Debug.", Loglevels.Debug, DateTime.Now, "John");
            Assert.AreEqual(logger.EntryCount, 1);
        }

        [Test]
        public void AddDebugIncrementsEntriCount()
        {
            logger.AddDebug("Need debug.", "Carlos");
            Assert.AreEqual(logger.EntryCount, 1);
            Assert.AreEqual(logger.HasDebug, true);
        }

        [Test]
        public void AddErrorIncrementsEntriCount()
        {
            logger.AddError("Found an error.", "Jon");
            Assert.AreEqual(logger.EntryCount, 1);
            Assert.AreEqual(logger.HasError, true);
        }

        [Test]
        public void AddWarningIncrementsEntriCount()
        {
            logger.AddWarning("New Warning.", "Peter");
            Assert.AreEqual(logger.EntryCount, 1);
            Assert.AreEqual(logger.HasWarning, true);
        }

        [Test]
        public void FlushResetsEntryCount()
        {
            logger.Add(testLog);
            logger.Flush();
            Assert.AreEqual(logger.EntryCount, 0);
        }

        [Test]
        public void HasDebugWorksAsExpected()
        {
            logger.AddDebug("Added debug", "David");
            Assert.AreEqual(logger.HasDebug, true);
        }

        [Test]
        public void HasErrorWorksAsExpected()
        {
            logger.AddError("Added Error", "David");
            Assert.AreEqual(logger.HasError, true);
        }

        [Test]
        public void HasWarningWorksAsExpected()
        {
            logger.AddWarning("Added warning", "David");
            Assert.AreEqual(logger.HasWarning, true);
        }

        [Test]
        public void HasInfoWorksAsExpected()
        {
            logger.AddDebug("Added debug", "David");
            logger.AddError("Added error", "David");
            logger.AddWarning("Added warning", "David");
            Assert.AreEqual(logger.HasInfo, false);
        }

        [Test]
        public void EntryCountWorksAsExpected()
        {
            Assert.AreEqual(logger.EntryCount, 0);
            logger.Add(testLog);
            Assert.AreEqual(logger.EntryCount, 1);
        }
    }
}
