﻿using System;
using static Shared.Enums;

namespace Shared
{
    public class Entities
    {
        public class Log
        {
            public DateTime TimeStamp { get; set; }
            public Loglevels Level { get; set; }
            public string UserName { get; set; }
            public string Text { get; set; }
        }
    }
}
